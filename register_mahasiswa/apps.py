from django.apps import AppConfig


class RegisterMahasiswaConfig(AppConfig):
    name = 'register_mahasiswa'
