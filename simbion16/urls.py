"""simbion16 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic.base import RedirectView 
from django.contrib import admin
import login.urls as login
import home.urls as home
import register_mahasiswa.urls as registermahasiswa
import register_donatur.urls as registerdonatur
import description.urls as description
import register_skema_beasiswa.urls as registersb
import list_beasiswa.urls as listbeasiswa
import calon_penerima.urls as calonpenerima 
import daftar_calon.urls as daftarcalon

from home.views import index as index_login


urlpatterns = [
    url(r'^$', include(home,namespace='home')),
    #url(r'^$', RedirectView.as_view(url="/home/",permanent=True), name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(login,namespace='login')),
    url(r'^registermahasiswa/', include(registermahasiswa,namespace='registermahasiswa')),
    url(r'^registerdonatur/', include(registerdonatur,namespace='registerdonatur')),
    url(r'^description/', include(description,namespace='description')),
    url(r'^registersb/', include(registersb,namespace='registersb')),
    url(r'^listbeasiswa/', include(listbeasiswa,namespace='listbeasiswa')),
    url(r'^calonpenerima/', include(calonpenerima,namespace='calonpenerima')),
    url(r'^daftarcalon/', include(daftarcalon,namespace='daftarcalon'))
]
