from django.shortcuts import render

# Create your views here.

def baru(request):
    response = {}
    return render(request, 'buat_beasiswa_baru.html', response)

def paket(request):
    response = {}
    return render(request, 'buat_beasiswa_dari_paket.html', response)