from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest

# Create your tests here.

class HomeUnitTest(TestCase):

    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)